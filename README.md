# Configuración inicial
1. Instala Git [aqui](https://git-scm.com/).
2. Instala Sourcetree [aqui](https://www.sourcetreeapp.com/). Esto facilita el manejo de Git.
3. Asegurate que estas en el [repositorio original](https://bitbucket.org/vivaorganicadata/datapipelines/src/master/) y hazle click en "Fork this repository".

    ![Crear fork parte 1](Imagenes/CrearForkParte1.png)

4. Deja el mismo nombre y asegurate que no este seleccionada la opcion de repositorio privado. De esta manera otros podran tener acceso a tu fork del repositorio, que es conveniente cuando alguien quiere incorporar tus cambios.

    ![Crear fork parte 2](Imagenes/CrearForkParte2.png)
  
5. Asegurate que estas en la pagina de tu fork, y presiona el boton de "Clone". Si estas usando una computadora Windows, es más sencillo usar la opcions HTTPS, si no se recomienda SSH. Copia el texto y ejecutalo en cmd o terminal. El texto sera similar a este: 

    ```
    git clone https://jvillalpando@bitbucket.org/jvillalpando/datapipelines.git
    ```

6. Navega en cmd al folder **DataPipelines**. 
7. Ahora regresa al [repositorio original](https://bitbucket.org/vivaorganicadata/datapipelines/src/master/) y picale "Clone" otra vez. Copia la direccion HTTPS (o SSH) aqui:

    ```
    git remote add upstream <URL_REPOSITORIO_ORIGINAL>
    ```
    
    Ejecuta el comando que se parecera a este:
    
    ```
    git remote add upstream https://jvillalpando@bitbucket.org/vivaorganicadata/datapipelines.git
    ```

8. Desabilita la habilidad de empujar cambios directamente al repositorio original por medio de este comando:
    
    ```
    git remote set-url upstream --push "No se permite subir directamente sin PR"
    ```
    
9. Verifica que todo se instalo adecuadamente ejecutando `git remote -v` y obteniendo resultados similares:
    
        origin	https://jvillalpando@bitbucket.org/jvillalpando/datapipelines.git (fetch)
        origin	https://jvillalpando@bitbucket.org/jvillalpando/datapipelines.git (push)
        upstream	https://jvillalpando@bitbucket.org/vivaorganicadata/datapipelines.git (fetch)
        upstream	No se permite subir directamente sin PR (push)
    
10. Agrega las siguientes opciones para facilitar el uso de Git: 

        git config --global user.name "<CUENTA_CORREO_BIT_BUCKET>"
        git config credential.helper store

11. Finalmente, prueba si la conexion a ambos repositorios fue configurada correctamente. Al jalar cambios del servidor, se te pedira que proporciones tu contraseña, pero debido a la opcion configurada anteriormente, se grabaran y en el futuro no tendras que volver a ingresarla.

        git fetch origin
        git fetch upstream

# Guía para crear Pull Requests (PRs)
Cada vez que se quieran hacer cambios al repositorio, se tendra que seguir los pasos a continuación. Si tienes dudas de ciertos comandos la
[documentación](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control) de Git es un buen recurso para consultar. Tambien el [tutorial](https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud) de Atlassian es excelente.

1. Primero, crea un nuevo branch que contendra los cambios que quieres hacer. Tambien tendras que asegurate que esta en el cambio mas actual del branch `master` del repositorio original (referido como `upstream`). 

        # Guarda los cambios que tienes localmente
        git stash
        # Jala los ultimos cambios en el repositorio original
        git fetch upstream
        # Crea un nuevo branch
        git checkout -b nombre_branch
        # Muevelo al ultimo cambio del branch master en el repositorio original. Elimina todos los commits adicionales que pudieras tener.    
        git rebase -i upstream/master
        # Recupera los cambios locales que guardaste al inicio
        git stash pop
    
    NOTA: El comando `git rebase -i` abrirá un archivo de texto que se tiene que editar manualmente.
     Si requires mas información de esto [aquí](https://www.atlassian.com/git/tutorials/rewriting-history/git-rebase) hay un buen tutorial.

2. Haz los cambios adecuados y crea uno o varios commits. Sourcetree es una manera facil de hacer esto.
3. Ya que todos los commits se hayan subido al servidor, crea un PR:
    1. Ve a tu fork del repositorio y hazle click a "Pull Requests". En la esquina superior derecha a un link para crear uno nuevo, presionalo.
    2. Asegurate que escoges el branch adecuado, y que el destino es el branch `master` de `vivaorganica/datapipelines`. Agrega titulo, descripción, y aquellas personas que quieren que te evaluen los cambios. Aqui se muestra un ejemplo de esto:
    
        ![Ejemplo crear PR](Imagenes/CrearPullRequest.png)
     
    3. Presiona el boton de "Create pull request".

Ahora podran revisar tus cambios y proporcionar comentarios. Todos los cambios adicionales que hagas a tu branch se agregaran al PR siempre cuando los empujes al servidor (es decir, no solo los tengas localmente).

## Merge de PR aprobado
Ya que tu PR sea aprobado, en la pagina del PR le podras picar al boton *Merge* (esquina derecha superior) para que los cambios sean parte del repositorio. Cuando le picas al boton, sale la pantalla siguiente, y es importante asegurarse de hacer los siguientes pasos:

1. Cambia el mensaje a algo apropiado que sea facil de leer. Quita el mensaje inicial que se parece a este: `Merged in jvillalpando/datapipelines/documentacion_inicial (pull request #1)`.
2. Asegurate que el "Merge strategy" es *Squash*.
3. Escoge la opcion de "Close source branch" para que automaticamente borre el branch que creaste para este PR.

![Ejemplo merge PR](Imagenes/MergePullRequest.png)

## Best Practices
Aquí hay unos buenos ejemplos de tips y *best practices*: 

- https://www.atlassian.com/blog/git/written-unwritten-guide-pull-requests
- https://github.com/alphagov/styleguides/blob/master/pull-requests.md
- https://blog.github.com/2015-01-21-how-to-write-the-perfect-pull-request/ 
- Markup cheat sheet - https://bitbucket.org/tutorials/markdowndemo/src/master/README.md

# Redshift
A continuación hay una serie de links con temas utiles acerca de Redshift:

- [Overview](https://docs.aws.amazon.com/redshift/latest/dg/c_redshift_system_overview.html)
- [Introducción al diseño de tablas](https://docs.aws.amazon.com/redshift/latest/dg/t_Creating_tables.html)
- [Sintaxis para crear tabla](https://docs.aws.amazon.com/redshift/latest/dg/r_CREATE_TABLE_NEW.html)
- [Tipo de datos](https://docs.aws.amazon.com/redshift/latest/dg/c_Supported_data_types.html)
- [Diseño avanzado de tablas](https://aws.amazon.com/blogs/big-data/amazon-redshift-engineerings-advanced-table-design-playbook-preamble-prerequisites-and-prioritization/)
- [Pasos para autorizar acceso cluster a otros servicios AWS](https://docs.aws.amazon.com/redshift/latest/mgmt/authorizing-redshift-service.html)
- [Herramientas recomendadas para conectarte a Redshift](https://www.blendo.co/amazon-redshift-guide-data-analyst/import-and-export-data/tools-to-connect-to-amazon-redshift-cluster/)
    - [Documentacion de AWS acerda de herramientas](https://docs.aws.amazon.com/redshift/latest/mgmt/connecting-via-client-tools.html)
