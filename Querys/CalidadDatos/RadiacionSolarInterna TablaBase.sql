SELECT d.Fecha,
       (CASE WHEN RegistrosUnicos IS NULL THEN 0 ELSE (100 * (RegistrosUnicos / (3.0 * 288))) END) Completeness,
       (CASE WHEN RegistrosTotales IS NULL THEN 0 ELSE (100 * (((RegistrosTotales * 4.0) - ErroresValidez) / (RegistrosTotales * 4.0))) END) Validez,
       (CASE WHEN RegistrosUnicos IS NULL THEN 0 ELSE (100 * ((RegistrosUnicos - RegistrosDuplicados) / (1.0 * RegistrosUnicos))) END) Unicidad,
       ErroresValidez, Lotes, (3 - Lotes) LotesFaltantes, LotesCompletos, RegistrosTotales, RegistrosUnicos, RegistrosDuplicados, DuplicadosTotales,
       rad_SolarIncorrecta, rad_SolarNula, rad_Solar_altaIncorrecta, rad_Solar_altaNula, rad_Solar_promedioIncorrecta, rad_Solar_promedioNula, rad_Solar_acumuladaIncorrecta, rad_Solar_acumuladaNula
FROM
  (SELECT Full_Date AS Fecha FROM Dim_Date WHERE Fecha BETWEEN DATEADD(DAY,-80,CURRENT_DATE) AND CURRENT_DATE) AS d
LEFT OUTER JOIN
  (SELECT Fecha, COUNT(*) Lotes, SUM(Registros) RegistrosTotales, SUM(RegistrosFaltantes) RegistrosFaltantes, SUM(CASE WHEN RegistrosFaltantes = 0 THEN 1 ELSE 0 END) LotesCompletos,
          SUM(CasosRad_SolarIncorrecta) rad_SolarIncorrecta, SUM(Casosrad_SolarNula) rad_SolarNula,
          SUM(Casosrad_Solar_altaIncorrecta) rad_Solar_altaIncorrecta, SUM(Casosrad_Solar_altaNula) rad_Solar_altaNula,
          SUM(CasosDefrad_Solar_promedioIncorrecta) rad_Solar_promedioIncorrecta, SUM(CasosDefrad_Solar_promedioNula) rad_Solar_promedioNula,
          SUM(Casosrad_Solar_acumuladaIncorrecta) rad_Solar_acumuladaIncorrecta, SUM(Casosrad_Solar_acumuladaNula) rad_Solar_acumuladaNula,
          (rad_SolarIncorrecta + rad_SolarNula + rad_Solar_altaIncorrecta + rad_Solar_altaNula + rad_Solar_promedioIncorrecta + rad_Solar_promedioNula + rad_Solar_acumuladaIncorrecta + rad_Solar_acumuladaNula) AS ErroresValidez
  FROM
    (SELECT Lote, Unidad_Negocio, Fecha, COUNT(*) Registros, (1440 - COUNT(*)) RegistrosFaltantes,
            SUM(CASE WHEN rad_Solar < 0 OR rad_Solar > 1000 THEN 1 ELSE 0 END) CasosRad_SolarIncorrecta, SUM(CASE WHEN rad_Solar IS NULL THEN 1 ELSE 0 END) Casosrad_SolarNula,
            SUM(CASE WHEN rad_Solar_alta < 0 OR rad_Solar_alta > 1000 THEN 1 ELSE 0 END) Casosrad_Solar_altaIncorrecta, SUM(CASE WHEN rad_Solar_alta IS NULL THEN 1 ELSE 0 END) Casosrad_Solar_altaNula,
            SUM(CASE WHEN rad_Solar_promedio < 0 OR rad_Solar_promedio > 800 THEN 1 ELSE 0 END) CasosDefrad_Solar_promedioIncorrecta, SUM(CASE WHEN rad_Solar_promedio IS NULL THEN 1 ELSE 0 END) CasosDefrad_Solar_promedioNula,
            SUM(CASE WHEN rad_Solar_acumulada < 0 OR rad_Solar_acumulada > 2000 THEN 1 ELSE 0 END) Casosrad_Solar_acumuladaIncorrecta, SUM(CASE WHEN rad_Solar_acumulada IS NULL THEN 1 ELSE 0 END) Casosrad_Solar_acumuladaNula
    FROM RadiacionSolarInterna
    GROUP BY Lote, Unidad_Negocio, Fecha) AS t
  GROUP BY Fecha) AS v
ON v.Fecha = d.Fecha
LEFT OUTER JOIN
  (SELECT Fecha, COUNT(*) RegistrosUnicos, SUM(CASE WHEN Registros > 1 THEN 1 ELSE 0 END) RegistrosDuplicados, SUM(Registros - 1) DuplicadosTotales
  FROM
    (SELECT Lote, Unidad_Negocio, Fecha, Hora, COUNT(*) Registros
    FROM RadiacionSolarInterna
    GROUP BY Lote, Unidad_Negocio, Fecha, Hora) AS t
  GROUP BY Fecha) AS u
ON u.Fecha = v.Fecha
ORDER BY d.Fecha;
