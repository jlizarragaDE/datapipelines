SELECT d.Fecha,
       (CASE WHEN RegistrosUnicos IS NULL THEN 0 ELSE (100 * (RegistrosUnicos / (3.0 * 288))) END) Completeness,
       (CASE WHEN RegistrosTotales IS NULL THEN 0 ELSE (100 * (((RegistrosTotales * 4.0) - ErroresValidez) / (RegistrosTotales * 4.0))) END) Validez,
       (CASE WHEN RegistrosUnicos IS NULL THEN 0 ELSE (100 * ((RegistrosUnicos - RegistrosDuplicados) / (1.0 * RegistrosUnicos))) END) Unicidad,
       ErroresValidez, Lotes, (3 - Lotes) LotesFaltantes, LotesCompletos, RegistrosTotales, RegistrosUnicos, RegistrosDuplicados, DuplicadosTotales,
       humedad_suelo_6inIncorrecta, humedad_suelo_6inNula, humedad_suelo_12inIncorrecta, humedad_suelo_12inNula, ce_6inIncorrecta, ce_6inNula, ce_12inIncorrecta, ce_12inNula
FROM
  (SELECT Full_Date AS Fecha FROM Dim_Date WHERE Fecha BETWEEN DATEADD(DAY,-50,CURRENT_DATE) AND CURRENT_DATE) AS d
LEFT OUTER JOIN
  (SELECT Fecha, COUNT(*) Lotes, SUM(Registros) RegistrosTotales, SUM(RegistrosFaltantes) RegistrosFaltantes, SUM(CASE WHEN RegistrosFaltantes = 0 THEN 1 ELSE 0 END) LotesCompletos,
          SUM(CasosHumedadSueloIncorrecta) humedad_suelo_6inIncorrecta, SUM(Casoshumedad_suelo_6inNula) humedad_suelo_6inNula,
          SUM(Casoshumedad_suelo_12inIncorrecta) humedad_suelo_12inIncorrecta, SUM(Casoshumedad_suelo_12inNula) humedad_suelo_12inNula,
          SUM(CasosDefce_6inIncorrecta) ce_6inIncorrecta, SUM(CasosDefce_6inNula) ce_6inNula,
          SUM(Casosce_12inIncorrecta) ce_12inIncorrecta, SUM(Casosce_12inNula) ce_12inNula,
          (humedad_suelo_6inIncorrecta + humedad_suelo_6inNula + humedad_suelo_12inIncorrecta + humedad_suelo_12inNula + ce_6inIncorrecta + ce_6inNula + ce_12inIncorrecta + ce_12inNula) AS ErroresValidez
  FROM
    (SELECT Lote, Unidad_Negocio, Fecha, COUNT(*) Registros, (1440 - COUNT(*)) RegistrosFaltantes,
            SUM(CASE WHEN humedad_suelo_6in < 0 OR humedad_suelo_6in > 680 THEN 1 ELSE 0 END) CasosHumedadSueloIncorrecta, SUM(CASE WHEN humedad_suelo_6in IS NULL THEN 1 ELSE 0 END) Casoshumedad_suelo_6inNula,
            SUM(CASE WHEN humedad_suelo_12in < 0 OR humedad_suelo_12in > 680 THEN 1 ELSE 0 END) Casoshumedad_suelo_12inIncorrecta, SUM(CASE WHEN humedad_suelo_12in IS NULL THEN 1 ELSE 0 END) Casoshumedad_suelo_12inNula,
            SUM(CASE WHEN ce_6in < 0 OR ce_6in > 4 THEN 1 ELSE 0 END) CasosDefce_6inIncorrecta, SUM(CASE WHEN ce_6in IS NULL THEN 1 ELSE 0 END) CasosDefce_6inNula,
            SUM(CASE WHEN ce_12in < 0 OR ce_12in > 4 THEN 1 ELSE 0 END) Casosce_12inIncorrecta, SUM(CASE WHEN ce_12in IS NULL THEN 1 ELSE 0 END) Casosce_12inNula
    FROM HumedadSuelo
    GROUP BY Lote, Unidad_Negocio, Fecha) AS t
  GROUP BY Fecha) AS v
ON v.Fecha = d.Fecha
LEFT OUTER JOIN
  (SELECT Fecha, COUNT(*) RegistrosUnicos, SUM(CASE WHEN Registros > 1 THEN 1 ELSE 0 END) RegistrosDuplicados, SUM(Registros - 1) DuplicadosTotales
  FROM
    (SELECT Lote, Unidad_Negocio, Fecha, Hora, COUNT(*) Registros
    FROM HumedadSuelo
    GROUP BY Lote, Unidad_Negocio, Fecha, Hora) AS t
  GROUP BY Fecha) AS u
ON u.Fecha = v.Fecha
ORDER BY d.Fecha;
