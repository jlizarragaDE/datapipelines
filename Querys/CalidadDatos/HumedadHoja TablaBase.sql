SELECT d.Fecha,
       (CASE WHEN RegistrosUnicos IS NULL THEN 0 ELSE (100 * (RegistrosUnicos / (3.0 * 288))) END) Completeness,
       (CASE WHEN RegistrosTotales IS NULL THEN 0 ELSE (100 * (((RegistrosTotales * 4.0) - ErroresValidez) / (RegistrosTotales * 4.0))) END) Validez,
       (CASE WHEN RegistrosUnicos IS NULL THEN 0 ELSE (100 * ((RegistrosUnicos - RegistrosDuplicados) / (1.0 * RegistrosUnicos))) END) Unicidad,
       ErroresValidez, Lotes, (3 - Lotes) LotesFaltantes, LotesCompletos, RegistrosTotales, RegistrosUnicos, RegistrosDuplicados, DuplicadosTotales,
       Humedad_HojaIncorrecta, Humedad_HojaNula, Humedad_Hoja_AltaIncorrecta, Humedad_Hoja_AltaNula, Humedad_Hoja_BajaIncorrecta, Humedad_Hoja_BajaNula, Humedad_Hoja_minIncorrecta, Humedad_Hoja_minNula
FROM
  (SELECT Full_Date AS Fecha FROM Dim_Date WHERE Fecha BETWEEN DATEADD(DAY,-80,CURRENT_DATE) AND CURRENT_DATE) AS d
LEFT OUTER JOIN
  (SELECT Fecha, COUNT(*) Lotes, SUM(Registros) RegistrosTotales, SUM(RegistrosFaltantes) RegistrosFaltantes, SUM(CASE WHEN RegistrosFaltantes = 0 THEN 1 ELSE 0 END) LotesCompletos,
          SUM(CasosHumedadHojaIncorrecta) Humedad_HojaIncorrecta, SUM(CasosHumedadHojaNula) Humedad_HojaNula,
          SUM(CasosHumedad_Hoja_AltaIncorrecta) Humedad_Hoja_AltaIncorrecta, SUM(CasosHumedad_Hoja_altaNula) Humedad_Hoja_AltaNula,
          SUM(CasosDefHumedad_Hoja_bajaIncorrecta) Humedad_Hoja_BajaIncorrecta, SUM(CasosDefHumedad_Hoja_bajaNula) Humedad_Hoja_BajaNula,
          SUM(CasosHumedad_Hoja_minIncorrecta) Humedad_Hoja_minIncorrecta, SUM(CasosHumedad_Hoja_minNula) Humedad_Hoja_minNula,
          (Humedad_HojaIncorrecta + Humedad_HojaNula + Humedad_Hoja_AltaIncorrecta + Humedad_Hoja_AltaNula + Humedad_Hoja_BajaIncorrecta + Humedad_Hoja_BajaNula + Humedad_Hoja_minIncorrecta + Humedad_Hoja_minNula) AS ErroresValidez
  FROM
    (SELECT Lote, Unidad_Negocio, Fecha, COUNT(*) Registros, (1440 - COUNT(*)) RegistrosFaltantes,
            SUM(CASE WHEN Humedad_Hoja < 0 OR Humedad_Hoja > 16 THEN 1 ELSE 0 END) CasosHumedadHojaIncorrecta, SUM(CASE WHEN Humedad_Hoja IS NULL THEN 1 ELSE 0 END) CasosHumedadHojaNula,
            SUM(CASE WHEN Humedad_Hoja_alta < 0 OR Humedad_Hoja_alta > 16 THEN 1 ELSE 0 END) CasosHumedad_Hoja_AltaIncorrecta, SUM(CASE WHEN Humedad_Hoja_alta IS NULL THEN 1 ELSE 0 END) CasosHumedad_Hoja_altaNula,
            SUM(CASE WHEN Humedad_Hoja_baja < 0 OR Humedad_Hoja_baja > 16 THEN 1 ELSE 0 END) CasosDefHumedad_Hoja_bajaIncorrecta, SUM(CASE WHEN Humedad_Hoja_baja IS NULL THEN 1 ELSE 0 END) CasosDefHumedad_Hoja_bajaNula,
            SUM(CASE WHEN Humedad_Hoja_min < 0 OR Humedad_Hoja_min > 8 THEN 1 ELSE 0 END) CasosHumedad_Hoja_minIncorrecta, SUM(CASE WHEN Humedad_Hoja_min IS NULL THEN 1 ELSE 0 END) CasosHumedad_Hoja_minNula
    FROM HumedadHoja
    GROUP BY Lote, Unidad_Negocio, Fecha) AS t
  GROUP BY Fecha) AS v
ON v.Fecha = d.Fecha
LEFT OUTER JOIN
  (SELECT Fecha, COUNT(*) RegistrosUnicos, SUM(CASE WHEN Registros > 1 THEN 1 ELSE 0 END) RegistrosDuplicados, SUM(Registros - 1) DuplicadosTotales
  FROM
    (SELECT Lote, Unidad_Negocio, Fecha, Hora, COUNT(*) Registros
    FROM HumedadHoja
    GROUP BY Lote, Unidad_Negocio, Fecha, Hora) AS t
  GROUP BY Fecha) AS u
ON u.Fecha = v.Fecha
ORDER BY d.Fecha;
