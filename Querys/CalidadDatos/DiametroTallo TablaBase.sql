SELECT d.Fecha,
       (CASE WHEN RegistrosUnicos IS NULL THEN 0 ELSE (100 * (RegistrosUnicos / (3.0 * 576))) END) Completeness,
       (CASE WHEN RegistrosTotales IS NULL THEN 0 ELSE (100 * (((RegistrosTotales * 1.0) - ErroresValidez) / (RegistrosTotales * 1.0))) END) Validez,
       (CASE WHEN RegistrosUnicos IS NULL THEN 0 ELSE (100 * ((RegistrosUnicos - RegistrosDuplicados) / (1.0 * RegistrosUnicos))) END) Unicidad,
       ErroresValidez, Lotes, (3 - Lotes) LotesFaltantes, LotesCompletos, RegistrosTotales, RegistrosUnicos, RegistrosDuplicados, DuplicadosTotales,
       DiametroIncorrecto, Diametro_Tallo_Nula
FROM
  (SELECT Full_Date AS Fecha FROM Dim_Date WHERE Fecha BETWEEN DATEADD(DAY,-100,CURRENT_DATE) AND CURRENT_DATE) AS d
LEFT OUTER JOIN
  (SELECT Fecha, COUNT(*) Lotes, SUM(Registros) RegistrosTotales, SUM(RegistrosFaltantes) RegistrosFaltantes, SUM(CASE WHEN RegistrosFaltantes = 0 THEN 1 ELSE 0 END) LotesCompletos,
          SUM(CasosDiametroTalloIncorrecta) DiametroIncorrecto, SUM(CasosDiametro_TalloNula) Diametro_Tallo_Nula,
          (DiametroIncorrecto + Diametro_Tallo_Nula) AS ErroresValidez
  FROM
    (SELECT Lote, Unidad_Negocio, Fecha, COUNT(*) Registros, (576 - COUNT(*)) RegistrosFaltantes,
            SUM(CASE WHEN Diametro_Tallo < 9 OR Diametro_Tallo > 14 THEN 1 ELSE 0 END) CasosDiametroTalloIncorrecta, SUM(CASE WHEN Diametro_Tallo IS NULL THEN 1 ELSE 0 END) CasosDiametro_TalloNula
    FROM DiametroTallo
    GROUP BY Lote, Unidad_Negocio, Fecha) AS t
  GROUP BY Fecha) AS v
ON v.Fecha = d.Fecha
LEFT OUTER JOIN
  (SELECT Fecha, COUNT(*) RegistrosUnicos, SUM(CASE WHEN Registros > 1 THEN 1 ELSE 0 END) RegistrosDuplicados, SUM(Registros - 1) DuplicadosTotales
  FROM
    (SELECT Lote, Unidad_Negocio, Fecha, Hora, COUNT(*) Registros
    FROM DiametroTallo
    GROUP BY Lote, Unidad_Negocio, Fecha, Hora) AS t
  GROUP BY Fecha) AS u
ON u.Fecha = v.Fecha
ORDER BY d.Fecha;
