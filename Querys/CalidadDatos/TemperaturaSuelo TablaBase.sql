SELECT d.Fecha,
       (CASE WHEN RegistrosUnicos IS NULL THEN 0 ELSE (100 * (RegistrosUnicos / (3.0 * 288))) END) Completeness,
       (CASE WHEN RegistrosTotales IS NULL THEN 0 ELSE (100 * (((RegistrosTotales * 2.0) - ErroresValidez) / (RegistrosTotales * 2.0))) END) Validez,
       (CASE WHEN RegistrosUnicos IS NULL THEN 0 ELSE (100 * ((RegistrosUnicos - RegistrosDuplicados) / (1.0 * RegistrosUnicos))) END) Unicidad,
       ErroresValidez, Lotes, (3 - Lotes) LotesFaltantes, LotesCompletos, RegistrosTotales, RegistrosUnicos, RegistrosDuplicados, DuplicadosTotales,
       Temperatura_6inIncorrecta, Temperatura_6inNula, Temperatura_12inIncorrecta, Temperatura_12inNula
FROM
  (SELECT Full_Date AS Fecha FROM Dim_Date WHERE Fecha BETWEEN DATEADD(DAY,-50,CURRENT_DATE) AND CURRENT_DATE) AS d
LEFT OUTER JOIN
  (SELECT Fecha, COUNT(*) Lotes, SUM(Registros) RegistrosTotales, SUM(RegistrosFaltantes) RegistrosFaltantes, SUM(CASE WHEN RegistrosFaltantes = 0 THEN 1 ELSE 0 END) LotesCompletos,
          SUM(CasosTemperatura_6inIncorrecta) Temperatura_6inIncorrecta, SUM(CasosTemperatura_6inNula) Temperatura_6inNula,
          SUM(CasosTemperatura_12inIncorrecta) Temperatura_12inIncorrecta, SUM(CasosTemperatura_12inNula) Temperatura_12inNula,
          (Temperatura_6inIncorrecta + Temperatura_6inNula + Temperatura_12inIncorrecta + Temperatura_12inNula) AS ErroresValidez
  FROM
    (SELECT Lote, Unidad_Negocio, Fecha, COUNT(*) Registros, (1440 - COUNT(*)) RegistrosFaltantes,
            SUM(CASE WHEN Temperatura_6in < 0 OR Temperatura_6in > 35 THEN 1 ELSE 0 END) CasosTemperatura_6inIncorrecta, SUM(CASE WHEN Temperatura_6in IS NULL THEN 1 ELSE 0 END) CasosTemperatura_6inNula,
            SUM(CASE WHEN Temperatura_12in < 0 OR Temperatura_12in > 35 THEN 1 ELSE 0 END) CasosTemperatura_12inIncorrecta, SUM(CASE WHEN Temperatura_12in IS NULL THEN 1 ELSE 0 END) CasosTemperatura_12inNula
    FROM TemperaturaSuelo
    GROUP BY Lote, Unidad_Negocio, Fecha) AS t
  GROUP BY Fecha) AS v
ON v.Fecha = d.Fecha
LEFT OUTER JOIN
  (SELECT Fecha, COUNT(*) RegistrosUnicos, SUM(CASE WHEN Registros > 1 THEN 1 ELSE 0 END) RegistrosDuplicados, SUM(Registros - 1) DuplicadosTotales
  FROM
    (SELECT Lote, Unidad_Negocio, Fecha, Hora, COUNT(*) Registros
    FROM TemperaturaSuelo
    GROUP BY Lote, Unidad_Negocio, Fecha, Hora) AS t
  GROUP BY Fecha) AS u
ON u.Fecha = v.Fecha
ORDER BY d.Fecha;
